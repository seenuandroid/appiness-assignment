package com.example.appiness.data.dataSource

import com.example.appiness.data.model.RemoteDataModel
import retrofit2.Call
import retrofit2.http.GET

interface APIInterface {
    @GET("api.json")
    fun getRemoteData(): Call<List<RemoteDataModel>>
}
