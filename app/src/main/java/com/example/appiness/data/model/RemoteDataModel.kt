package com.example.appiness.data.model


import com.google.gson.annotations.SerializedName

data class RemoteDataModel(
    @SerializedName("country")
    val country: String? = "",
    @SerializedName("end.time")
    val endTime: String? = null,
    @SerializedName("s.no")
    val sNo: Int? = null,
    @SerializedName("num.backers")
    val numBackers: String? = null,
    @SerializedName("blurb")
    val blurb: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("amt.pledged")
    val amtPledged: Int? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("by")
    val by: String? = null,
    @SerializedName("currency")
    val currency: String? = null,
    @SerializedName("location")
    val location: String? = null,
    @SerializedName("state")
    val state: String? = null,
    @SerializedName("percentage.funded")
    val percentageFunded: Int? = null
)


