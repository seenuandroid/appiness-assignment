package com.example.appiness.data.feature

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.appiness.data.common.isNetworkAvailable
import com.example.appiness.data.model.RemoteDataModel
import com.example.appiness.data.repo.RemoteDataRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MainViewModel(app: Application) : AndroidViewModel(app) {
    private val repo by lazy {
        RemoteDataRepo(app)
    }
    val obsList = MutableLiveData<List<RemoteDataModel>>()
    val obsError = MutableLiveData<String>().apply { postValue(null) }
    val obsPreogress = MutableLiveData<Boolean>().apply { postValue(false) }

    private val job = Job()
    private val ioScope = CoroutineScope(Dispatchers.IO + job)

    init {
        getRemoteData()
    }


    fun getRemoteData() {
        obsError.postValue(null)

        if (isNetworkAvailable(getApplication())) {
            ioScope.launch {
                obsPreogress.postValue(true)
                repo.getRemoteData {
                    obsPreogress.postValue(false)
                    it?.let {
                        Log.d(TAG, "remoteData $it")
                        obsList.postValue(it)
                        job.cancel()
                    }
                        ?: obsError.postValue(
                            "Server message: ex: servers are not responding please try again"
                        )
                }
            }
        } else {
            obsError.postValue("Please connect to internet")
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    companion object {
        val TAG = MainViewModel::class.java.simpleName
    }
}