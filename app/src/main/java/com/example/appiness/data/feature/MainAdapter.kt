package com.example.appiness.data.feature

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.appiness.data.model.RemoteDataModel
import com.example.appiness.databinding.ItemDataBinding

class MainAdapter : ListAdapter<RemoteDataModel, MainAdapter.Holder>(DIFF) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemDataBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class Holder(private val itemListBinding: ItemDataBinding) :
        RecyclerView.ViewHolder(itemListBinding.root) {
        fun bind(item: RemoteDataModel) {
            itemListBinding.vm = item
        }
    }


    companion object {
        private val DIFF =
            object : DiffUtil.ItemCallback<RemoteDataModel>() {
                override fun areItemsTheSame(
                    oldItem: RemoteDataModel,
                    newItem: RemoteDataModel
                ): Boolean {
                    return oldItem == newItem
                }

                override fun areContentsTheSame(
                    oldItem: RemoteDataModel,
                    newItem: RemoteDataModel
                ): Boolean {
                    return oldItem.sNo == newItem.sNo
                }
            }
    }

}