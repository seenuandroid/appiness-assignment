package com.example.appiness.data.feature

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appiness.R
import com.example.appiness.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val mViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private val mAdapter by lazy {
        MainAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(
            ActivityMainBinding.inflate(LayoutInflater.from(this)).apply {
                lifecycleOwner = this@MainActivity
                vm = mViewModel
            }.root
        )
        init()
    }

    private fun init() {
        recyclerView?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = mAdapter
        }
        mViewModel.obsList.observe(this, Observer {
            mAdapter.submitList(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val searchView = menu?.findItem(R.id.app_bar_search)?.actionView as SearchView
        searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isNotEmpty()) {
                    val list = mViewModel.obsList.value?.filter {
                        it.title?.toLowerCase()?.contains(newText.toLowerCase()) == true
                    }
                    mAdapter.submitList(list)
                } else {
                    mAdapter.submitList(mViewModel.obsList.value)
                }
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }
}
