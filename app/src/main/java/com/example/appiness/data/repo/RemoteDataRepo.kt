package com.example.appiness.data.repo

import android.content.Context
import com.example.appiness.data.BaseDataSource
import com.example.appiness.data.dataSource.RetrofitDataSource
import com.example.appiness.data.model.RemoteDataModel

class RemoteDataRepo(context: Context) : BaseDataSource {
    private val remoteDataSource by lazy {
        RetrofitDataSource(context)
    }

    override fun getRemoteData(res: (List<RemoteDataModel>?) -> Unit) {
        remoteDataSource.getRemoteData(res)
    }
}