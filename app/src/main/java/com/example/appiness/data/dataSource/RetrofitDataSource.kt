package com.example.appiness.data.dataSource

import android.content.Context
import android.util.Log
import com.example.appiness.R
import com.example.appiness.data.BaseDataSource
import com.example.appiness.data.model.RemoteDataModel
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitDataSource(context: Context) :
    BaseDataSource {

    private val baseUrl = context.getString(R.string.baseUrl)
    private val retrofitService =
        getRetrofitService(baseUrl).create(APIInterface::class.java)

    override fun getRemoteData(res: (List<RemoteDataModel>?) -> Unit) {
        val call = retrofitService.getRemoteData()
        call.enqueue(object : Callback<List<RemoteDataModel>> {
            override fun onFailure(call: Call<List<RemoteDataModel>>?, t: Throwable?) {
                Log.e(TAG, "Failed to retrieve Data")
                res(null)
            }

            override fun onResponse(
                call: Call<List<RemoteDataModel>>?,
                response: Response<List<RemoteDataModel>>?
            ) {
                Log.d(TAG, "Successfully retrieved.")
                if (response?.isSuccessful == true) {
                    response.body()?.let { res(it) }
                } else {
                    res(null)
                }
            }
        })
    }


    companion object {
        val TAG = RetrofitDataSource::class.java.simpleName
        fun getRetrofitService(baseUrl: String): Retrofit {
            val client = OkHttpClient.Builder().addInterceptor(
                HttpLoggingInterceptor().setLevel(
                    HttpLoggingInterceptor.Level.BODY
                )
            )
            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            return retrofit
        }
    }

}
