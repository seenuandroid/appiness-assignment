package com.example.appiness.data

import com.example.appiness.data.model.RemoteDataModel

interface BaseDataSource {
    fun getRemoteData(res: (List<RemoteDataModel>?) -> Unit){
        throw Exception("Not Implemented")
    }
}
